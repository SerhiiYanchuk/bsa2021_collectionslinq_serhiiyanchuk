﻿using BSA2021_CollectionsLINQ.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BSA2021_CollectionsLINQ
{
    public static class Factory
    {
        private static HttpResponseMessage GetData(HttpClient httpClient, string route)
        {
            var response = httpClient.GetAsync($"https://bsa21.azurewebsites.net/api/{route}").Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception($"Status: {response.StatusCode}, {response.Content}");
            return response;
        }
        public static (List<Project> projects, List<Task> tasks, List<Team> teams, List<User> users) LoadDataFromServer()
        {
            List<Project> projects = new List<Project>();
            List<Task> tasks = new List<Task>();
            List<Team> teams = new List<Team>();
            List<User> users = new List<User>();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = GetData(client, "Projects");
                projects = JsonConvert.DeserializeObject<List<Project>>(response.Content.ReadAsStringAsync().Result);

                response = GetData(client, "Tasks");
                tasks = JsonConvert.DeserializeObject<List<Task>>(response.Content.ReadAsStringAsync().Result);

                response = GetData(client, "Teams");
                teams = JsonConvert.DeserializeObject<List<Team>>(response.Content.ReadAsStringAsync().Result);

                response = GetData(client, "Users");
                users = JsonConvert.DeserializeObject<List<User>>(response.Content.ReadAsStringAsync().Result);
            }

            return (projects, tasks, teams, users);
        }
        public static List<Project> CreateStructureQuerySyntax(List<Project> projects, List<Task> tasks, List<Team> teams, List<User> users)
        {
            var tasksWithPerformers = from task in tasks
                                      join performer in users on task.PerformerId equals performer.Id
                                      select new Task
                                      {
                                          Id = task.Id,
                                          ProjectId = task.ProjectId,
                                          PerformerId = task.PerformerId,
                                          Performer = performer,
                                          Name = task.Name,
                                          Description = task.Description,
                                          State = task.State,
                                          CreatedAt = task.CreatedAt,
                                          FinishedAt = task.FinishedAt,
                                      };

            return (from project in projects
                    join task in tasksWithPerformers on project.Id equals task.ProjectId into taskList
                    join author in users on project.AuthorId equals author.Id
                    join team in teams on project.TeamId equals team.Id
                    join member in users on team.Id equals member.TeamId into memberList
                    select new Project
                    {
                        Id = project.Id,
                        AuthorId = project.AuthorId,
                        Author = author,
                        TeamId = project.TeamId,
                        Team = new Team
                        {
                            Id = team.Id,
                            Name = team.Name,
                            CreatedAt = team.CreatedAt,
                            Members = memberList.ToList()
                        },
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        Tasks = taskList.ToList()
                    }).ToList();
        }
        public static List<Project> CreateStructureMethodSyntax(List<Project> projects, List<Task> tasks, List<Team> teams, List<User> users)
        {
            var tasksWithPerformers = tasks.Join(users,
                                                 task => task.PerformerId,
                                                 performer => performer.Id,
                                                 (t, p) => new Task
                                                 {
                                                     Id = t.Id,
                                                     ProjectId = t.ProjectId,
                                                     PerformerId = t.PerformerId,
                                                     Performer = p,
                                                     Name = t.Name,
                                                     Description = t.Description,
                                                     State = t.State,
                                                     CreatedAt = t.CreatedAt,
                                                     FinishedAt = t.FinishedAt,
                                                 });

            return projects.GroupJoin(tasksWithPerformers,
                                      project => project.Id,
                                      task => task.ProjectId,
                                      (p, t) => new
                                      {
                                          Project = p,
                                          Tasks = t
                                      })
                            .Join(users,
                                      g => g.Project.AuthorId,
                                      author => author.Id,
                                      (g, a) => new
                                      {
                                          Project = g.Project,
                                          Tasks = g.Tasks,
                                          Author = a
                                      })
                            .Join(teams,
                                      g => g.Project.TeamId,
                                      team => team.Id,
                                      (g, t) => new
                                      {
                                          Project = g.Project,
                                          Tasks = g.Tasks,
                                          Author = g.Author,
                                          Team = t
                                      })
                            .GroupJoin(users,
                                      g => g.Team.Id,
                                      member => member.TeamId,
                                      (g, m) => new
                                      {
                                          Project = g.Project,
                                          Tasks = g.Tasks,
                                          Author = g.Author,
                                          Team = g.Team,
                                          TeamMembers = m
                                      })
                            .Select(g => new Project
                            {
                                Id = g.Project.Id,
                                AuthorId = g.Project.AuthorId,
                                Author = g.Author,
                                TeamId = g.Project.TeamId,
                                Team = new Team
                                {
                                    Id = g.Team.Id,
                                    Name = g.Team.Name,
                                    CreatedAt = g.Team.CreatedAt,
                                    Members = g.TeamMembers.ToList()
                                },
                                Name = g.Project.Name,
                                Description = g.Project.Description,
                                Deadline = g.Project.Deadline,
                                Tasks = g.Tasks.ToList()
                            })
                            .ToList();
        }
        
    }
}
