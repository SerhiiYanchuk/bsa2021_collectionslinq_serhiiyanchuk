﻿using BSA2021_CollectionsLINQ.Entities;
using BSA2021_CollectionsLINQ.EqualityComparer;
using BSA2021_CollectionsLINQ.QueryResultEntities;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BSA2021_CollectionsLINQ
{
    public static class LinqMethodSyntax
    {
        public static Dictionary<Project, int> SelectMethodSyntax1(this List<Project> projects, int performerId)
        {
            return projects.SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                            .Where(p => p.Task.PerformerId == performerId)
                            .GroupBy(p => p.Project, new ProjectEqualityComparer())
                            .ToDictionary(g => g.Key, g => g.Count());
        }
        public static List<Task> SelectMethodSyntax2(this List<Project> projects, int performerId)
        {
            const int MaxNameLength = 45;
            return projects.SelectMany(p => p.Tasks)
                           .Where(t => t.PerformerId == performerId && t.Name.Length < MaxNameLength)
                           .ToList();
        }
        public static List<FinishedTask> SelectMethodSyntax3(this List<Project> projects, int performerId)
        {
            const int CurrentYear = 2021;
            return projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == performerId)
                            .Where(t => t.FinishedAt.HasValue && t.FinishedAt.Value.Year == CurrentYear)
                            .Select(t => new FinishedTask
                            {
                                Id = t.Id,
                                Name = t.Name
                            })
                            .ToList();
        }
        public static List<TeamShortInfo> SelectMethodSyntax4(this List<Project> projects)
        {
            const int MinAge = 10;
            return projects.Select(p => p.Team)
                           .Distinct(new TeamEqualityComparer())
                           .Where(t => t.Members.All(m => DateTime.Now.Year - m.BirthDay.Year >= MinAge))
                           .Select(t => new TeamShortInfo
                           {
                               Id = t.Id,
                               Name = t.Name,
                               Members = t.Members.OrderByDescending(m => m.RegisteredAt).ToList()
                           })
                          .ToList();
        }
        public static List<PerformerWithTasks> SelectMethodSyntax5(this List<Project> projects)
        {
            return projects.SelectMany(p => p.Tasks)
                           .GroupBy(t => t.Performer, new UserEqualityComparer())
                           .Select(g => new PerformerWithTasks
                           {
                               Id = g.Key.Id,
                               TeamId = g.Key.TeamId,
                               FirstName = g.Key.FirstName,
                               LastName = g.Key.LastName,
                               Email = g.Key.Email,
                               RegisteredAt = g.Key.RegisteredAt,
                               BirthDay = g.Key.BirthDay,
                               Tasks = g.OrderByDescending(t => t.Name.Length).ToList()
                           })
                           .OrderBy(performer => performer.FirstName)
                           .ToList();
        }
        public static UserInfo SelectMethodSyntax6(this List<Project> projects, int userId)
        {
            return projects.Where(p => p.AuthorId == userId)
                           .GroupBy(p => p.Author, new UserEqualityComparer())
                           .Select(g => new
                           {
                               User = g.Key,
                               LastProject = g.OrderByDescending(p => p.CreatedAt).First(),
                               UserTasks = projects.SelectMany(p => p.Tasks).Where(t => t.PerformerId == g.Key.Id)
                           })
                           .Select(g => new UserInfo
                           {
                               User = g.User,
                               LastProject = g.LastProject,
                               ProjectTasksCount = g.LastProject.Tasks.Count(),
                               UserUnfinishedTasksCount = g.UserTasks.Count(t => !t.FinishedAt.HasValue),
                               LongestTask = g.UserTasks.OrderByDescending(t => t.FinishedAt.HasValue ? (t.FinishedAt - t.CreatedAt) : (DateTime.Now - t.CreatedAt)).FirstOrDefault()
                           })
                           .FirstOrDefault();
        }
        public static List<ProjectInfo> SelectMethodSyntax7(this List<Project> projects)
        {
            const int MinDescriptionLength = 20;
            const int MaxProjectTasks = 3;
            return projects.Select(p => new ProjectInfo
                           {
                               Project = p,
                               LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                               ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                               TeamMembersCount = p.Description.Length > MinDescriptionLength || p.Tasks.Count() < MaxProjectTasks ? p.Team.Members.Count() : new int?()
                           })
                           .ToList();
        }
    }
}
