﻿using BSA2021_CollectionsLINQ.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace BSA2021_CollectionsLINQ.EqualityComparer
{
    public class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] User obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
