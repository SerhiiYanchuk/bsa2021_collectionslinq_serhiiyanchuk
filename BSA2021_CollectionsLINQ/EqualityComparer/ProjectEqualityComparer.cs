﻿using BSA2021_CollectionsLINQ.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace BSA2021_CollectionsLINQ.EqualityComparer
{
    public class ProjectEqualityComparer : IEqualityComparer<Project>
    {
        public bool Equals(Project x, Project y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] Project obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
