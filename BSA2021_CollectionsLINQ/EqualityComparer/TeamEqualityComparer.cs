﻿using BSA2021_CollectionsLINQ.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace BSA2021_CollectionsLINQ.EqualityComparer
{
    public class TeamEqualityComparer : IEqualityComparer<Team>
    {
        public bool Equals(Team x, Team y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] Team obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
