﻿using BSA2021_CollectionsLINQ.Entities;
using BSA2021_CollectionsLINQ.EqualityComparer;
using BSA2021_CollectionsLINQ.QueryResultEntities;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BSA2021_CollectionsLINQ
{
    public static class LinqQuerySyntax
    {
        public static Dictionary<Project, int> SelectQuerySyntax1(this List<Project> projects, int performerId)
        {
            return (from project in projects
                          from task in project.Tasks
                          where task.PerformerId == performerId
                          group task by project into g
                          select new
                          {
                              Project = g.Key,
                              TaskCount = g.Count()
                          })
                          .ToDictionary(g => g.Project, g => g.TaskCount); ;
        }
        public static List<Task> SelectQuerySyntax2(this List<Project> projects, int performerId)
        {
            const int MaxNameLength = 45;
            return (from project in projects
                          from task in project.Tasks
                          where task.PerformerId == performerId && task.Name.Length < MaxNameLength
                    select task)
                          .ToList();
        }
        public static List<FinishedTask> SelectQuerySyntax3(this List<Project> projects, int performerId)
        {
            const int CurrentYear = 2021;
            return (from project in projects
                          from task in project.Tasks
                          where task.PerformerId == performerId
                          where task.FinishedAt.HasValue && task.FinishedAt.Value.Year == CurrentYear
                          select new FinishedTask
                          {
                              Id = task.Id,
                              Name = task.Name
                          })
                          .ToList();
        }
        public static List<TeamShortInfo> SelectQuerySyntax4(this List<Project> projects)
        {
            const int MinAge = 10;
            return (from team in projects.Select(p => p.Team).Distinct(new TeamEqualityComparer())
                          where team.Members.All(m => DateTime.Now.Year - m.BirthDay.Year >= MinAge)
                          select new TeamShortInfo
                          {
                              Id = team.Id,
                              Name = team.Name,
                              Members = team.Members.OrderByDescending(m => m.RegisteredAt).ToList()
                          })
                          .ToList();

        }
        public static List<PerformerWithTasks> SelectQuerySyntax5(this List<Project> projects)
        {
            return (from project in projects
                          from task in project.Tasks
                          group task by task.Performer into g
                          orderby g.Key.FirstName
                          select new PerformerWithTasks
                          {
                              Id = g.Key.Id,
                              TeamId = g.Key.TeamId,
                              FirstName = g.Key.FirstName,
                              LastName = g.Key.LastName,
                              Email = g.Key.Email,
                              RegisteredAt = g.Key.RegisteredAt,
                              BirthDay = g.Key.BirthDay,
                              Tasks = (from task in g orderby task.Name.Length descending select task).ToList()
                          })
                          .ToList();
        }
        public static UserInfo SelectQuerySyntax6(this List<Project> projects, int userId)
        {
            return (from project in projects
                          where project.AuthorId == userId
                          group project by project.Author into g
                          let lastProject = (from project in g orderby project.CreatedAt descending select project).First()
                          let userTasks = (from project in projects
                                           from task in project.Tasks
                                           where task.PerformerId == g.Key.Id select task)
                          select new UserInfo
                          {
                              User = g.Key,
                              LastProject = lastProject,
                              ProjectTasksCount = lastProject.Tasks.Count(),
                              UserUnfinishedTasksCount = userTasks.Count(t => !t.FinishedAt.HasValue),
                              LongestTask = userTasks.OrderByDescending(t => t.FinishedAt.HasValue ? (t.FinishedAt - t.CreatedAt) : (DateTime.Now - t.CreatedAt)).FirstOrDefault()
                          })
                          .FirstOrDefault();
        }

        public static List<ProjectInfo> SelectQuerySyntax7(this List<Project> projects)
        {
            const int MinDescriptionLength = 20;
            const int MaxProjectTasks = 3;
            return (from project in projects
                          select new ProjectInfo
                          {
                              Project = project,
                              LongestDescriptionTask = (from task in project.Tasks orderby task.Description.Length descending select task).FirstOrDefault(),
                              ShortestNameTask = (from task in project.Tasks orderby task.Name.Length select task).FirstOrDefault(),
                              TeamMembersCount = project.Description.Length > MinDescriptionLength || project.Tasks.Count() < MaxProjectTasks ? project.Team.Members.Count() : new int?()
                          })
                          .ToList();
        }
    }
}
