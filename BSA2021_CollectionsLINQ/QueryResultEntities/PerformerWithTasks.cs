﻿using BSA2021_CollectionsLINQ.Entities;
using System.Collections.Generic;

namespace BSA2021_CollectionsLINQ.QueryResultEntities
{
    public class PerformerWithTasks: User
    {
        public List<Task> Tasks { get; set; } = new List<Task>();
    }
}
