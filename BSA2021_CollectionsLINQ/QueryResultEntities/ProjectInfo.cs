﻿using BSA2021_CollectionsLINQ.Entities;



namespace BSA2021_CollectionsLINQ.QueryResultEntities
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Task LongestDescriptionTask { get; set; }
        public Task ShortestNameTask { get; set; }
        public int? TeamMembersCount { get; set; }
    }
}
