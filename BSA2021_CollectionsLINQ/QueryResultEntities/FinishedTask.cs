﻿

namespace BSA2021_CollectionsLINQ.QueryResultEntities
{
    public class FinishedTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
