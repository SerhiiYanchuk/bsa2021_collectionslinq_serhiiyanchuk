﻿using BSA2021_CollectionsLINQ.Entities;



namespace BSA2021_CollectionsLINQ.QueryResultEntities
{
    public class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int ProjectTasksCount { get; set; }
        public int UserUnfinishedTasksCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
