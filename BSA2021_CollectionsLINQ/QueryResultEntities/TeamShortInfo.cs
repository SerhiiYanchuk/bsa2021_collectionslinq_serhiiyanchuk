﻿using BSA2021_CollectionsLINQ.Entities;
using System.Collections.Generic;

namespace BSA2021_CollectionsLINQ.QueryResultEntities
{
    public class TeamShortInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Members { get; set; } = new List<User>();
    }
}
