﻿using System;


namespace BSA2021_CollectionsLINQ.Entities
{
    public class User
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        // methods are overridden for "group by" using query syntax LINQ, since cannot specify own EqualityComparer as in method syntax
        public override bool Equals(object obj)
        {
            if (obj is User item)
                return this.Id.Equals(item.Id);

            return false;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}


