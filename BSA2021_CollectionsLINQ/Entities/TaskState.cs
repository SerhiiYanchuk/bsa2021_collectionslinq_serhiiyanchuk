﻿

namespace BSA2021_CollectionsLINQ.Entities
{
    public enum TaskState: byte
    {
        Undefined = 0,
        Unfinished,
        Finished,
        Canceled
    }
}
