﻿using System;
using System.Collections.Generic;


namespace BSA2021_CollectionsLINQ.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Members { get; set; } = new List<User>();

        // methods are overridden for "group by" using query syntax LINQ, since cannot specify own EqualityComparer as in method syntax
        public override bool Equals(object obj)
        {
            if (obj is Team item)
                return this.Id.Equals(item.Id);

            return false;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}
