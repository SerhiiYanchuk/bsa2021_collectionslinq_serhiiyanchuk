﻿using System;
using System.Collections.Generic;


namespace BSA2021_CollectionsLINQ.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }      
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<Task> Tasks { get; set; } = new List<Task>();

        // methods are overridden for "group by" using query syntax LINQ, since cannot specify own EqualityComparer as in method syntax
        public override bool Equals(object obj)
        {
            if (obj is Project item)
                return this.Id.Equals(item.Id);

            return false;         
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}
