﻿using System;


namespace BSA2021_CollectionsLINQ.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; } 
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        // methods are overridden for "group by" using query syntax LINQ, since cannot specify own EqualityComparer as in method syntax
        public override bool Equals(object obj)
        {
            if (obj is Task item)
                return this.Id.Equals(item.Id);

            return false;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}
