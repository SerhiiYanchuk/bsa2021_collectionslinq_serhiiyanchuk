﻿using BSA2021_CollectionsLINQ.Entities;
using BSA2021_CollectionsLINQ.QueryResultEntities;
using System;
using System.Collections.Generic;


namespace BSA2021_CollectionsLINQ
{
    class Program
    {
        enum QueryType: byte
        {
            MethodSyntax = 1,
            QuerySyntax
        }
        static void Main(string[] args)
        {
            var (projects, tasks, teams, users) = Factory.LoadDataFromServer();
            var data = Factory.CreateStructureMethodSyntax(projects, tasks, teams, users);

            while (true)
            {
                Console.Clear();
                Console.WriteLine("-----------------------");
                Console.WriteLine($"{' ',-3}LINQ & Collections\n{' ',-3}Main menu");
                Console.WriteLine("-----------------------\n");
                Console.WriteLine("1 - Method syntax");
                Console.WriteLine("2 - Query syntax");

                Console.WriteLine("0 - Exit");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            SubMenu(data, QueryType.MethodSyntax);
                            break;
                        case 2:
                            SubMenu(data, QueryType.QuerySyntax);
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                }
            }


        }
        static void SubMenu(List<Project> data, QueryType queryType)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("----------------------");
                if (queryType == QueryType.MethodSyntax)
                    Console.WriteLine($"{' ',-3}LINQ & Collections\n{' ',-3}Method syntax");
                else
                    Console.WriteLine($"{' ',-3}LINQ & Collections\n{' ',-3}Query syntax");
             
                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).");
                Console.WriteLine("2 - Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).");
                Console.WriteLine("3 - Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id).");
                Console.WriteLine("4 - Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.");
                Console.WriteLine("5 - Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию)");
                Console.WriteLine(@"6 - Получить следующую структуру (передать Id пользователя в параметры):
                                        User
                                        Последний проект пользователя(по дате создания)
                                        Общее кол - во тасков под последним проектом
                                        Общее кол - во незавершенных или отмененных тасков для пользователя
                                        Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)");
                Console.WriteLine(@"7 - Получить следующую структуру:
                                        Проект
                                        Самый длинный таск проекта (по описанию)
                                        Самый короткий таск проекта (по имени)
                                        Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3");
                Console.WriteLine("0 - Back");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            Console.Write("Id пользователя > ");
                            Dictionary<Project, int> selection1 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax1(int.Parse(Console.ReadLine())) : data.SelectQuerySyntax1(int.Parse(Console.ReadLine()));
                           
                            Console.WriteLine();
                            foreach (var item in selection1)
                                Console.WriteLine($"{"", 5}Проект: {item.Key.Name,-35} К-ство тасков: {item.Value}");
                            break;
                        case 2:
                            Console.Write("Id пользователя > ");                            
                            List<Task> selection2 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax2(int.Parse(Console.ReadLine())) : data.SelectQuerySyntax2(int.Parse(Console.ReadLine()));
                           
                            Console.WriteLine();
                            foreach (var item in selection2)
                                Console.WriteLine($"{"",5}Таск: {item.Name, -52} Длина названия: {item.Name.Length}");
                            break;
                        case 3:
                            Console.Write("Id пользователя > ");                                                    
                            List<FinishedTask> selection3 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax3(int.Parse(Console.ReadLine())) : data.SelectQuerySyntax3(int.Parse(Console.ReadLine()));
                            
                            Console.WriteLine();
                            foreach (var item in selection3)
                                Console.WriteLine($"{"",5}Id: {item.Id,-5} Название таска: {item.Name}");
                            break;
                        case 4:
                            List<TeamShortInfo> selection4 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax4() : data.SelectQuerySyntax4();

                            Console.WriteLine();
                            foreach (var item in selection4)
                            {
                                Console.WriteLine($"{"",5}Id: {item.Id,-5} Команда: {item.Name}");
                                foreach (var item2 in item.Members)
                                    Console.WriteLine($"{"",10}Фамилия: {item2.LastName,-20} Год рождения: {item2.BirthDay.Year, -10}  Дата регистрации: {item2.RegisteredAt,-20}");
                                Console.WriteLine();
                            }    
                                
                            break;
                        case 5:
                            List<PerformerWithTasks> selection5 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax5() : data.SelectQuerySyntax5();

                            Console.WriteLine();
                            foreach (var item in selection5)
                            {
                                Console.WriteLine($"{"",5}Имя исполнителя: {item.FirstName}");
                                foreach (var item2 in item.Tasks)
                                    Console.WriteLine($"{"",10}Название таска: {item2.Name}");
                                Console.WriteLine();
                            }
                            break;
                        case 6:
                            Console.Write("Id пользователя > ");
                            UserInfo selection6 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax6(int.Parse(Console.ReadLine())) : data.SelectQuerySyntax6(int.Parse(Console.ReadLine()));

                            if (selection6 is not null)
                            {
                                Console.WriteLine($"\n{"",5}Пользователь: {selection6.User.FirstName + " " + selection6.User.LastName}");
                                Console.WriteLine($"{"",10}Дата последнего проекта: {selection6.LastProject.CreatedAt}");
                                Console.WriteLine($"{"",10}К-ство таксов последнего проекта: {selection6.ProjectTasksCount}");
                                Console.WriteLine($"{"",10}К-ство незвершенных тасков пользователя: {selection6.UserUnfinishedTasksCount}");
                                string end = selection6.LongestTask.FinishedAt.HasValue ? selection6.LongestTask.FinishedAt.ToString() : "до этих пор";
                                Console.WriteLine($"{"",10}Самый долгий таск пользователя: {selection6.LongestTask.CreatedAt} - {end}");
                            }
                            
                            break;
                        case 7:
                            List<ProjectInfo> selection7 = queryType == QueryType.MethodSyntax ? data.SelectMethodSyntax7() : data.SelectQuerySyntax7();

                            foreach(var item in selection7)
                            {
                                Console.WriteLine($"\n{"",5}Проект: {item.Project.Name}");
                                Console.WriteLine($"{"",10}Описание самого длинного таска по описанию: {item.LongestDescriptionTask?.Description ?? "Тасков нет", -50}");
                                Console.WriteLine($"{"",10}Имя самого короткого такса по имени: {item.ShortestNameTask?.Name ?? "Тасков нет",-50}");
                                string count = item.TeamMembersCount.HasValue ? item.TeamMembersCount.Value.ToString() : "не выполнилось условие \"описание проекта > 20 символов или кол-во тасков < 3\"";
                                Console.WriteLine($"{"",10}Общее кол-во пользователей в команде проекта: {count,-50}");
                            }
                            break;
                        case 0:
                            return;
                        default:
                            Console.Write("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
    }




}
